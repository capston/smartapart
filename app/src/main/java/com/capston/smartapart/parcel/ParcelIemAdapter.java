package com.capston.smartapart.parcel;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.capston.smartapart.R;

import java.util.LinkedList;

/**
 * Created by ohyongtaek on 17. 1. 17..
 */
public class ParcelIemAdapter extends BaseAdapter {
    LinkedList<String[]> parcelList;
    Context context;
    LayoutInflater inflater;

    ParcelIemAdapter(Context context) {
        this.context = context;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.parcelList = new LinkedList<>();
    }
    @Override
    public int getCount() {
        return parcelList.size();
    }

    @Override
    public Object getItem(int position) {
        return parcelList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.parcel_item, parent, false);
        }
        TextView time = (TextView) convertView.findViewById(R.id.parcelTime);
        TextView period = (TextView) convertView.findViewById(R.id.period);
        time.setText(parcelList.get(position)[0]);
        period.setText(parcelList.get(position)[1]);
        return convertView;
    }

    public void addParcel(String time, String period) {
        parcelList.add(new String[] {time, period});
        notifyDataSetChanged();
    }
}
