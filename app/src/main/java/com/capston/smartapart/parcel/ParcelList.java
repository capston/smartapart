package com.capston.smartapart.parcel;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.capston.smartapart.R;

/**
 * Created by ohyongtaek on 17. 1. 17..
 */
public class ParcelList extends Fragment {

    View view;
    ListView listView;
    ParcelIemAdapter parcelAdapter;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.parcel_list, container, false);
        listView = (ListView) view.findViewById(R.id.parcel_list);

        parcelAdapter = new ParcelIemAdapter(view.getContext());
        testInit();
        listView.setAdapter(parcelAdapter);

        return view;
    }

    public void testInit() {
        parcelAdapter.addParcel("2016.12.06", "1일");
        parcelAdapter.addParcel("2016.12.06", "4일");
        parcelAdapter.addParcel("2016.12.03", "1일");
    }
}
