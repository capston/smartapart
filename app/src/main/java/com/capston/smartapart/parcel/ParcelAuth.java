package com.capston.smartapart.parcel;

import android.app.Fragment;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.GridView;
import android.widget.Spinner;

import com.capston.smartapart.R;
import com.capston.smartapart.contact.AccountItemAdapter;

/**
 * Created by ohyongtaek on 17. 1. 17..
 */
public class ParcelAuth extends Fragment {

    View view;
    Spinner authSpinner;
    Spinner startTime;
    Spinner endTime;
    GridView authGridView;
    Button cancelButton;
    Button permitButton;
    AccountItemAdapter accountItemAdapter;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.auth_send, container, false);

        authSpinner = (Spinner) view.findViewById(R.id.auth_spinner);
        startTime = (Spinner) view.findViewById(R.id.start_time);
        endTime = (Spinner) view.findViewById(R.id.end_time);
        authGridView = (GridView) view.findViewById(R.id.auth_gridview);
        cancelButton = (Button) view.findViewById(R.id.cancel_action);
        permitButton = (Button) view.findViewById(R.id.permit_action);

        accountItemAdapter = new AccountItemAdapter(view.getContext());

        accountItemAdapter.addAccount("동생");
        accountItemAdapter.addAccount("엄마");
        accountItemAdapter.addAccount("아빠");
        authGridView.setAdapter(accountItemAdapter);

        GradientDrawable drawable1 = (GradientDrawable) permitButton.getBackground();
        drawable1.setColor(getResources().getColor(R.color.permitColor));

        GradientDrawable drawable2 = (GradientDrawable) cancelButton.getBackground();
        drawable2.setColor(getResources().getColor(R.color.cancelColor));
        return view;
    }
}
