package com.capston.smartapart.models;

import android.support.v7.app.AppCompatActivity;

/**
 * Created by ohyongtaek on 17. 1. 17..
 */
public class Service {
    private String name;
    private AppCompatActivity activity;

    public Service(AppCompatActivity activity, String name) {
        this.activity = activity;
        this.name = name;
    }

    public AppCompatActivity getActivity() {
        return activity;
    }

    public void setActivity(AppCompatActivity activity) {
        this.activity = activity;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
