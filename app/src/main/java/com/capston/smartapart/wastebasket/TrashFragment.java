package com.capston.smartapart.wastebasket;

import android.app.Fragment;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Spinner;
import android.widget.TextView;

import com.capston.smartapart.Address;
import com.capston.smartapart.Base;
import com.capston.smartapart.R;
import com.capston.smartapart.Response;
import com.capston.smartapart.statistic.GetStatisticData;
import com.github.mikephil.charting.charts.LineChart;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import org.w3c.dom.Text;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

/**
 * Created by ohyongtaek on 16. 11. 26..
 */
public class TrashFragment extends Fragment {

    Gson gson = new GsonBuilder().create();
    private LineChart mChart;
    static String serviceServerUrl;
    static int addressId;


    com.capston.smartapart.statistic.LineChart lineChart;
    TextView recentDate;
    TextView recentWeight;
    TextView monthCharge;
    Spinner statisticSpinner;


    public static TrashFragment getInstence() {
        return new TrashFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.linechart_trash, container, false);

        recentDate = (TextView) view.findViewById(R.id.recentDate);
        recentWeight = (TextView) view.findViewById(R.id.recentWeight);
        monthCharge  = (TextView) view.findViewById(R.id.monthCharge);
        statisticSpinner = (Spinner) view.findViewById(R.id.statisticSpinner);

        return view;
    }

    public JsonArray creatJsonArray() {
        JsonArray array = new JsonArray();

        JsonObject object = new JsonObject();
        object.addProperty("weight",3);
        object.addProperty("time", "2016-03-16");

        array.add(object);

        JsonObject object1 = new JsonObject();
        object1.addProperty("weight",4);
        object1.addProperty("time", "2016-04-16");

        array.add(object1);

        JsonObject object2 = new JsonObject();
        object2.addProperty("weight", 5);
        object2.addProperty("time", "2016-05-16");

        array.add(object2);

        return array;
    }

//    class StatisticTask extends AsyncTask<String, Void, JsonObject> {
//        @Override
//        protected JsonObject doInBackground(String... tags) {
//            Log.d("test",tags[0]);
//            URL authUrl = null;
//            try {
//                authUrl = new URL(Base.authServer_url);
//            } catch (MalformedURLException e) {
//                e.printStackTrace();
//            }
//            Log.d("test",authUrl.toString());
//            String response = "";
//            JsonObject responseObject = null;
//            Response resultResponse = null;
//            try {
//                //auth_url = new URL(auth_url);
//                HttpURLConnection conn = (HttpURLConnection) authUrl.openConnection();
//                conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
//                conn.setReadTimeout(10000 /* milliseconds */);
//                conn.setConnectTimeout(15000 /* milliseconds */);
//                conn.setRequestMethod("POST");
//                conn.setDoInput(true);
//                conn.setDoOutput(true);
//                    //conn.connect()
//                OutputStream os = conn.getOutputStream(); // 서버로 보내기 위한 출력 스트림
////                BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(os, "UTF-8")); // UTF-8로 전송
//
//                Log.d("test",tags[0]);
//                String tag = URLEncoder.encode("tag", "UTF-8") + "=" + URLEncoder.encode(tags[0],"UTF-8");
//                os.write(tag.getBytes()); // 매개변수 전송
//                os.flush();
//                os.close();
//                if (conn.getResponseCode() == HttpURLConnection.HTTP_OK) { // 연결에 성공한 경우
//                    String line;
//                    BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream())); // 서버의 응답을 읽기 위한 입력 스트림
//
//                    while ((line = br.readLine()) != null) // 서버의 응답을 읽어옴
//                        response += line;
//                }
//
//                conn.disconnect();
//                    //response = conn.getResponseMessage();
//                Log.d("RESPONSE", "The response is: " + response);
//                resultResponse = gson.fromJson(response, Response.class);
//            } catch (IOException e) {
//                Log.d("test",e.getMessage());
//            }
//
//            serviceServerUrl = resultResponse.getServiceUrl();
//            addressId = resultResponse.getAddressUrl();
//
//            responseObject = gson.fromJson(new GetStatisticData().GetStatisticData(Base.userId, addressId, "", "", serviceServerUrl), JsonObject.class);
//
//            return responseObject;
//        }
//
//        @Override
//        protected void onPostExecute(JsonObject jsonObject) {
//            JsonObject lastData = jsonObject.get("last_data").getAsJsonObject();
//
//            String data = String.valueOf(lastData.get("weight").getAsDouble());
//            JsonArray array = jsonObject.get("statistic").getAsJsonArray();
//
//            textView.setText(data);
//            lineChart.createChart(array.toString(), "date");
//
//            super.onPostExecute(jsonObject);
//        }
//    }

}
