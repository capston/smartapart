package com.capston.smartapart.wastebasket;

import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.capston.smartapart.MainPageAdapter;
import com.capston.smartapart.R;
import com.capston.smartapart.parking.ParkingAuth;
import com.capston.smartapart.parking.ParkingList;

/**
 * Created by ohyongtaek on 17. 1. 19..
 */
public class TrashService extends AppCompatActivity {
    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;
    private Toolbar toolbar;
    ViewPager viewPager;
    MainPageAdapter mainPageAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setDrawer();
        mDrawerLayout = (DrawerLayout) findViewById(R.id.main_activity);
        mDrawerToggle = new ActionBarDrawerToggle(
                this,
                mDrawerLayout,
                toolbar,
                R.string.drawer_open,
                R.string.drawer_close
        ) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                invalidateOptionsMenu();
                syncState();
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                invalidateOptionsMenu();
                syncState();
            }
        };

        mDrawerLayout.addDrawerListener(mDrawerToggle);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        mDrawerToggle.syncState();


        viewPager = (ViewPager) findViewById(R.id.body);
        mainPageAdapter = new MainPageAdapter(getFragmentManager());
        mainPageAdapter.addFragment(TrashFragment.getInstence());
        mainPageAdapter.addFragment(new TrashAuth());

        mainPageAdapter.notifyDataSetChanged();
        viewPager.setAdapter(mainPageAdapter);


        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabDots);
        if (tabLayout != null) {
            tabLayout.setupWithViewPager(viewPager);
        }
    }

    public void setTitle(String title){
        ((TextView)findViewById(R.id.toolbar_title)).setText(title);
    }

    private void setDrawer() {
        GradientDrawable drawable1 = (GradientDrawable) findViewById(R.id.settingbtn).getBackground();
        drawable1.setColor(Color.parseColor("#ff0099cc"));
        GradientDrawable drawable2 = (GradientDrawable) findViewById(R.id.addressbtn).getBackground();
        drawable2.setColor(Color.parseColor("#ffff8800"));
    }
}
