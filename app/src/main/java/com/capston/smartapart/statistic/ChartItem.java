package com.capston.smartapart.statistic;

/**
 * Created by ohyongtaek on 16. 11. 26..
 */
public class ChartItem {
    private double data;
    private String date;

    public ChartItem() {
    }

    public ChartItem(double data, String date) {
        this.data = data;
        this.date = date;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public double getData() {
        return data;
    }

    public void setData(double data) {
        this.data = data;
    }


}
