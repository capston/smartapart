package com.capston.smartapart.statistic;

import android.content.Context;
import android.util.Log;

import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by ohyongtaek on 16. 11. 26..
 */
public class LineChart {
    private String title;
    private LineDataSet lineDataSet;
    private com.github.mikephil.charting.charts.LineChart lineChart;

    public LineChart(com.github.mikephil.charting.charts.LineChart lineChart, String title) {
        this.lineChart = lineChart;
        this.title = title;
    }

    public com.github.mikephil.charting.charts.LineChart getLineChart() {
        return lineChart;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public LineDataSet getLineDataSet() {
        return lineDataSet;
    }

    public void setLineDataSet(LineDataSet lineDataSet) {
        this.lineDataSet = lineDataSet;
    }

    public void setLineChart(com.github.mikephil.charting.charts.LineChart lineChart) {
        this.lineChart = lineChart;
    }

    public void setChart() {
        lineChart.setDrawGridBackground(false);
        lineChart.getDescription().setEnabled(true);

        lineChart.setDragEnabled(true);
        lineChart.setScaleEnabled(true);
//        Description description = new Description();
//        description.setText(title);
//        lineChart.setDescription(description);

        // if disabled, scaling can be done on x- and y-axis separately
        lineChart.setPinchZoom(true);

    }
    public void createChart(String json, String type) {
        setChart();
        LinkedList<ChartItem> rawDatas = new Parser().parseDataFromJson(json);
        if(type.equals("date")) {
            setDataByDate(rawDatas);
        }
        Log.d("test", "test");
    }

    public void setDataByDate(LinkedList<ChartItem> data) {
        Map<String,Double> dates = new HashMap<>();
        List<Entry> values = new LinkedList<>();
        for (ChartItem item : data) {
            String[] date = item.getDate().split(" ");
            if(!dates.containsKey(date[0])) {
                dates.put(date[0], item.getData());
            } else {
                dates.put(date[0], dates.get(date[0]) + item.getData());
            }
        }
        int i = 0;
        for (String key : dates.keySet()) {
            values.add(new Entry(i,dates.get(key).floatValue()));
            i++;
        }
        lineDataSet = new LineDataSet(values, title);
        attachDataSet();
    }
    public void attachDataSet() {
        ArrayList<ILineDataSet> lineDataSets = new ArrayList<>();
        lineDataSets.add(lineDataSet);
        LineData lineData = new LineData(lineDataSets);
        lineChart.setData(lineData);

    }

}
