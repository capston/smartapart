package com.capston.smartapart.statistic;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

/**
 * Created by vmffkxlgnqh1 on 2016. 11. 26..
 */
public class GetStatisticData {

    Gson gson = new GsonBuilder().create();

    public String GetStatisticData(String userId, int addressId, String startDate, String endDate, String url){
        URL urlobj = null;

        JsonObject obj = new JsonObject();
        obj.addProperty("userId",userId);
        obj.addProperty("addressId",addressId);
        if(startDate == null){
            obj.addProperty("startDate","");
        }
        else{
            obj.addProperty("startDate",startDate);
        }
        if(endDate != null){
            obj.addProperty("endDate", "");
        }
        else{
            obj.addProperty("endDate", endDate);
        }
        Log.d("json",gson.toJson(obj));

        try {
            urlobj = new URL(url);
            Log.d("test", urlobj.toString());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        if(urlobj != null){

            String response = "";
            try {
                HttpURLConnection con = (HttpURLConnection) urlobj.openConnection();
                con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
//                con.setRequestProperty("User-Agent", "Mozilla/5.0");
                con.setConnectTimeout(10000);
                con.setReadTimeout(30000);
                con.setDoOutput(true);
                con.setDoInput(true);
                con.setRequestMethod("POST");

                OutputStream wr = con.getOutputStream();
                String tag = "";
                tag +=  URLEncoder.encode("tag","UTF-8") + "=" + URLEncoder.encode(gson.toJson(obj),"UTF-8");
                wr.write(tag.getBytes());
                wr.flush();
                wr.close();
                Log.d("before","input");
                int result = con.getResponseCode();
                Log.d("result",result+"");
                if (result == HttpURLConnection.HTTP_OK) { // 연결에 성공한 경우
                    String line;
                    Log.d("success","it is success");

                    BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream())); // 서버의 응답을 읽기 위한 입력 스트림

                    while ((line = br.readLine()) != null) // 서버의 응답을 읽어옴
                        response += line;
                }

                Log.d("test", response.toString());

                return response.toString();


            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }
}
