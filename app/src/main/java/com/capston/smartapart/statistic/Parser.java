package com.capston.smartapart.statistic;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.util.Iterator;
import java.util.LinkedList;

/**
 * Created by ohyongtaek on 16. 11. 26..
 */
public class Parser {

    private Gson gson = new GsonBuilder().create();

    public LinkedList<ChartItem> parseDataFromJson(String jsonData) {

        JsonElement element = gson.fromJson(jsonData, JsonElement.class);
        if(element.isJsonArray()) {
            Iterator<JsonElement> iterator = element.getAsJsonArray().iterator();
            LinkedList<ChartItem> values = new LinkedList<>();
            while (iterator.hasNext()) {
                JsonElement jsonElement = iterator.next();
                JsonObject object = jsonElement.getAsJsonObject();
                double data = object.get("weight").getAsInt();
                String date = object.get("time").getAsString();
                values.add(new ChartItem(data, date));
            }
            return values;
        } else {
            return null;
        }
    }
}
