package com.capston.smartapart;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.support.v13.app.FragmentStatePagerAdapter;

import java.util.LinkedList;

/**
 * Created by ohyongtaek on 17. 1. 13..
 */
public class MainPageAdapter extends FragmentStatePagerAdapter {

    Context context;
    LinkedList<Fragment> fragments = new LinkedList<>();

    public MainPageAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        return fragments.get(position);
    }

    @Override
    public int getCount() {
        return fragments.size();
    }

    public void addFragment(Fragment fragment) {
        fragments.add(fragment);
    }
}
