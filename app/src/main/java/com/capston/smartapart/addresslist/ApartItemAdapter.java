package com.capston.smartapart.addresslist;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.capston.smartapart.R;

import java.util.HashMap;
import java.util.LinkedList;

/**
 * Created by ohyongtaek on 17. 1. 17..
 */
public class ApartItemAdapter extends BaseAdapter {

    LinkedList<String[]> apartList;
    Context context;
    LayoutInflater inflater;
    ApartItemAdapter(Context context) {
        this.context = context;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        apartList = new LinkedList<>();
    }
    @Override
    public int getCount() {
        return apartList.size();
    }

    @Override
    public Object getItem(int position) {
        return apartList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.apart_item, parent, false);
        }
        TextView apartName = (TextView) convertView.findViewById(R.id.apartName);
        TextView address = (TextView) convertView.findViewById(R.id.address);
        apartName.setText(apartList.get(position)[0]);
        address.setText(apartList.get(position)[1]);

        if(position == 0) {
            ImageView imageView = (ImageView) convertView.findViewById(R.id.currentApart);
            imageView.setImageResource(R.drawable.check);
        }

        return convertView;
    }

    public void addApart(String name, String address) {
        apartList.add(new String[] {name, address});
        notifyDataSetChanged();
    }
}
