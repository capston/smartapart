package com.capston.smartapart.addresslist;

import android.app.Fragment;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ListAdapter;
import android.widget.TextView;

import com.capston.smartapart.R;

/**
 * Created by ohyongtaek on 17. 1. 12..
 */
public class ApartsFragment extends Fragment {

    View view;
    GridView gridView;
    ApartItemAdapter apartListAdapter;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.apart_list, container, false);
        gridView = (GridView) view.findViewById(R.id.apart_view);
        apartListAdapter = new ApartItemAdapter(view.getContext());

        testInit();
        gridView.setAdapter(apartListAdapter);


        TextView apartName = (TextView) view.findViewById(R.id.available_service);
        apartName.setTypeface(Typeface.DEFAULT_BOLD);
        return view;
    }

    public void testInit() {
        apartListAdapter.addApart("현대 1차", "313동 402호");
        apartListAdapter.addApart("위브하늘채", "101동 301호");
        apartListAdapter.notifyDataSetChanged();
    }
}
