package com.capston.smartapart;

/**
 * Created by cheonyujung on 2016. 11. 26..
 */
public class Response {

    String service_url;

    int address_url;

    public Response(String service_url, int address_url) {
        this.service_url = service_url;
        this.address_url = address_url;
    }

    public String getServiceUrl() {
        return service_url;
    }

    public void setServiceUrl(String service_url) {
        this.service_url = service_url;
    }

    public int getAddressUrl() {
        return address_url;
    }

    public void setAddressUrl(int address_url) {
        this.address_url = address_url;
    }

}
