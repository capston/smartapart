package com.capston.smartapart.service;

import android.app.Fragment;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.capston.smartapart.R;
import com.capston.smartapart.models.Parcel;
import com.capston.smartapart.models.Parking;
import com.capston.smartapart.models.Service;
import com.capston.smartapart.models.Trash;
import com.capston.smartapart.parcel.ParcelService;
import com.capston.smartapart.parking.ParkingService;
import com.capston.smartapart.wastebasket.TrashService;

/**
 * Created by ohyongtaek on 17. 1. 13..
 */
public class ServiceList extends Fragment {

    View view;
    ListView serviceListView;
    ServiceListAdapter serviceListAdapter;
    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.service_list, container, false);
        serviceListView = (ListView) view.findViewById(R.id.service_list);
        serviceListAdapter = new ServiceListAdapter();
        serviceListView.setAdapter(serviceListAdapter);

        TextView textView = (TextView) view.findViewById(R.id.available_service);
        textView.setTypeface(Typeface.DEFAULT_BOLD);

        serviceListAdapter.addService(new Parcel(new ParcelService(), "무인 택배함"));
        serviceListAdapter.addService(new Parking(new ParkingService(), "주차장"));
        serviceListAdapter.addService(new Trash(new TrashService(), "쓰레기 종량기"));
        serviceListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Service service = (Service) serviceListAdapter.getItem(position);
                Intent intent = new Intent(getActivity(), service.getActivity().getClass());
                startActivity(intent);
            }
        });
        return view;
    }
}
