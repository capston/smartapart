package com.capston.smartapart.service;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.capston.smartapart.R;
import com.capston.smartapart.models.Service;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.LinkedList;

/**
 * Created by ohyongtaek on 17. 1. 13..
 */
public class ServiceListAdapter extends BaseAdapter {

    LinkedList<Service> services = new LinkedList<>();
    @Override
    public int getCount() {
        return services.size();
    }

    @Override
    public Object getItem(int position) {
        return services.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Context context = parent.getContext();
        if(convertView == null){
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.service_item, parent, false);
        }
        TextView textView = (TextView) convertView.findViewById(R.id.service_item);
        textView.setText(services.get(position).getName());
        return convertView;
    }
    public void addService(Service service) {
        services.add(service);
        notifyDataSetChanged();
    }
}
