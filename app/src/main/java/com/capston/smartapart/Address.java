package com.capston.smartapart;

/**
 * Created by ohyongtaek on 16. 11. 27..
 */
public class Address {

    String location;
    int dong;
    int ho;

    public Address(int dong, int ho, String location) {
        this.dong = dong;
        this.ho = ho;
        this.location = location;
    }

    public int getDong() {
        return dong;
    }

    public void setDong(int dong) {
        this.dong = dong;
    }

    public int getHo() {
        return ho;
    }

    public void setHo(int ho) {
        this.ho = ho;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }
}
