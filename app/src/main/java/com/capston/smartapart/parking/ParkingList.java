package com.capston.smartapart.parking;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.capston.smartapart.R;

/**
 * Created by ohyongtaek on 17. 1. 18..
 */
public class ParkingList extends Fragment {

    View view;
    ListView parkingListView;
    ParkingItemAdapter itemAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.parking_list, container, false);
        parkingListView = (ListView) view.findViewById(R.id.parking_list);

        itemAdapter = new ParkingItemAdapter(view.getContext());
        itemAdapter.addParkingCar("경기 23 가 1234", "A열 10");

        parkingListView.setAdapter(itemAdapter);
        return view;
    }
}
