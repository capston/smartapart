package com.capston.smartapart.parking;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.capston.smartapart.R;

import java.util.LinkedList;

/**
 * Created by ohyongtaek on 17. 1. 18..
 */
public class ParkingItemAdapter extends BaseAdapter {

    LinkedList<String[]> parkingList;
    Context context;
    LayoutInflater inflater;
    ParkingItemAdapter(Context context) {
        parkingList = new LinkedList<>();
        this.context = context;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return parkingList.size();
    }

    @Override
    public Object getItem(int position) {
        return parkingList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.parking_item, parent, false);
        }
        TextView name = (TextView) convertView.findViewById(R.id.car_name);
        TextView car_position = (TextView) convertView.findViewById(R.id.parking_position);
        name.setText(parkingList.get(position)[0]);
        car_position.setText(parkingList.get(position)[1]);

        return convertView;
    }

    public void addParkingCar(String name, String position) {
        parkingList.add(new String[] {name, position});
        notifyDataSetChanged();
    }
}
