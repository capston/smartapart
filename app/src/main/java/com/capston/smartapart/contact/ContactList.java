package com.capston.smartapart.contact;

import android.app.Fragment;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.TextView;

import com.capston.smartapart.R;

/**
 * Created by ohyongtaek on 17. 1. 13..
 */
public class ContactList extends Fragment {

    View view;
    ExpandableListView expandableListView;
    ContactAdapter adapter;
    DisplayMetrics metrics;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.contact_list, container, false);
        TextView contactText = (TextView) view.findViewById(R.id.contact);
        contactText.setTypeface(Typeface.DEFAULT_BOLD);
        expandableListView = (ExpandableListView) view.findViewById(R.id.contactList);
        adapter = new ContactAdapter(getActivity(), R.layout.contact_group, R.layout.contact_child);

        adapter.testInitialContact();
        expandableListView.setAdapter(adapter);
        expandableListView.setDivider(null);

        metrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int width = metrics.widthPixels;

        expandableListView.setIndicatorBounds(width - GetDipsFromPixel(90), width - GetDipsFromPixel(50));
        return view;
    }

    public int GetDipsFromPixel(float pixels)
    {
        // Get the screen's density scale
        final float scale = getResources().getDisplayMetrics().density;
        // Convert the dps to pixels, based on density scale
        return (int) (pixels * scale + 0.5f);
    }
}
