package com.capston.smartapart.contact;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import com.capston.smartapart.R;

import java.util.HashMap;
import java.util.LinkedList;

/**
 * Created by ohyongtaek on 17. 1. 13..
 */
public class ContactAdapter extends BaseExpandableListAdapter {

    private Context context;
    private int groupLayout = 0;
    private int childLayout = 0;
    private LayoutInflater myInflater;
    private LinkedList<String> groupList = new LinkedList<>();
    private HashMap<String, LinkedList<String>> contactList = new HashMap<>();

    ContactAdapter(Context context, int groupLayout, int childLayout) {
        this.groupLayout = groupLayout;
        this.childLayout = childLayout;
        this.context = context;
        this.myInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    @Override
    public int getGroupCount() {
        return groupList.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return contactList.get(groupList.get(groupPosition)).size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return groupList.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return contactList.get(groupList.get(groupPosition)).get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        if(convertView == null){
            convertView = myInflater.inflate(this.groupLayout, parent, false);
        }
        TextView groupName = (TextView)convertView.findViewById(R.id.groupName);
        groupName.setText(groupList.get(groupPosition));
        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        if(convertView == null){
            convertView = myInflater.inflate(this.childLayout, parent, false);
        }
        TextView childName = (TextView)convertView.findViewById(R.id.contactName);
        childName.setText(contactList.get(groupList.get(groupPosition)).get(childPosition));
        return convertView;

    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }

    public void testInitialContact() {
        groupList.add("가족");
        groupList.add("친구");
        groupList.add("직장 동료");

        contactList.put(groupList.get(0), new LinkedList<String>());
        contactList.put(groupList.get(1), new LinkedList<String>());
        contactList.put(groupList.get(2), new LinkedList<String>());

        contactList.get(groupList.get(0)).add("엄마");
        contactList.get(groupList.get(0)).add("아빠");
        contactList.get(groupList.get(0)).add("동생");

        contactList.get(groupList.get(1)).add("전병은");
        contactList.get(groupList.get(1)).add("최순영");
        contactList.get(groupList.get(1)).add("천유정");
        contactList.get(groupList.get(1)).add("박건우");
        contactList.get(groupList.get(1)).add("김지영");

    }
}
