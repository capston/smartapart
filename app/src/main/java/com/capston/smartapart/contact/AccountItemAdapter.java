package com.capston.smartapart.contact;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.capston.smartapart.R;

import java.util.LinkedList;

/**
 * Created by ohyongtaek on 17. 1. 18..
 */
public class AccountItemAdapter extends BaseAdapter {
    LinkedList<String> accountList;
    Context context;
    LayoutInflater inflater;
    public AccountItemAdapter(Context context) {
        accountList = new LinkedList<>();
        this.context = context;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return accountList.size();
    }

    @Override
    public Object getItem(int position) {
        return accountList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.account_box, parent, false);
        }
        TextView name = (TextView) convertView.findViewById(R.id.account_name);
        name.setText(accountList.get(position));
        convertView.setElevation(10);
        return convertView;
    }

    public void addAccount(String name) {
        accountList.add(name);
        notifyDataSetChanged();
    }
}
